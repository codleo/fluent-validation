using FluentValidation;

namespace Introduction
{
  public class ExampleValidator : AbstractValidator<Example>
  {
    public ExampleValidator()
    {
      RuleFor(e => e.Number)
        .GreaterThanOrEqualTo(10);
      RuleFor(e => e.Text)
        .NotEmpty();

      RuleFor(e => e.Text)
        .MaximumLength(100);

      RuleForEach(e => e.Collection)
        .NotNull();
    }
  }
}