using System.Collections.Generic;

namespace Introduction
{
  public class Example
  {
    public int Number { get; set; }
    public string Text { get; set; }
    public List<object> Collection { get; set; }
  }
}