﻿using System;
using System.Collections.Generic;
using FluentValidation;

namespace Introduction
{
  class Program
  {
    static void Main(string[] args)
    {
      ValidatorOptions.LanguageManager.Enabled = false;
      var example = new Example
      {
        Number = 1,
        Text = "",
        Collection = new List<object> { new { X = "x" }, null }
      };

      var validator = new ExampleValidator();

      Console.WriteLine("Validate:");
      var result = validator.Validate(example);
      Console.WriteLine(result);

      try
      {
        Console.WriteLine("ValidateAndThrow:");
        validator.ValidateAndThrow(example);
      }
      catch (ValidationException e)
      {
        Console.WriteLine(e.Message);
      }
    }
  }
}