using Xunit;
using FluentValidation.TestHelper;
using FluentValidation;

namespace Tests
{
  public class ValidatorTests
  {
    private TestValidator validator;

    public ValidatorTests()
    {
      validator = new TestValidator();
    }

    [Fact]
    public void Validator_ShouldHaveValidationErrorForText_WhenEmpty()
    {
      validator.ShouldHaveValidationErrorFor(x => x.Text, "");
    }

    [Fact]
    public void Validator_ShouldHaveValidationErrorForText_WhenModelWithTextEmpty()
    {
      validator.ShouldHaveValidationErrorFor(x => x.Text, new TestModel());
    }

    [Fact]
    public void Validator_NotEmptyValidatorErrorCode_WhenEmpty()
    {
      validator.ShouldHaveValidationErrorFor(x => x.Text, "")
        .WithErrorCode("NotEmptyErrorCode");
    }

    [Fact]
    public void Validator_NotEmptyMessage_WhenEmpty()
    {
      validator.ShouldHaveValidationErrorFor(x => x.Text, "")
        .WithErrorMessage("Text cannot be empty");
    }

    [Fact]
    public void Validator_ShouldNotHaveValidationErrorFor_WhenNotEmpty()
    {
      validator.ShouldNotHaveValidationErrorFor(x => x.Text, "text");
    }

    [Fact]
    public void Validator_ShouldHaveChildValidator()
    {
      validator.ShouldHaveChildValidator(x => x.Numbers, typeof(ChildTestValidator));
    }

    [Fact]
    public void Validator_NotEmpty()
    {
      validator.ShouldHaveValidationErrorFor(x => x.Numbers, new int[0]{})
        .WithErrorCode("NotEmptyValidator");
    }
  }

  internal class TestModel
  {
    public string Text { get; set; }
    public int[] Numbers { get; set; }
  }

  internal class TestValidator : AbstractValidator<TestModel>
  {
    public TestValidator()
    {
      RuleFor(x => x.Text)
        .NotEmpty()
        .WithErrorCode("NotEmptyErrorCode")
        .WithMessage("Text cannot be empty");

      RuleFor(x => x.Numbers).SetValidator(new ChildTestValidator());

      RuleFor(x => x.Numbers).NotEmpty();
    }
  }

  internal class ChildTestValidator : AbstractValidator<int[]>
  {
  }
}
