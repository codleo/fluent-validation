using FluentValidation;

namespace Extend.Condition
{
  public class Vehicle
  {
    public VehicleType Type { get; set; }
    public int Wheels { get; set; }
  }

  public class VehicleValidator : AbstractValidator<Vehicle>
  {
    public VehicleValidator()
    {
      RuleFor(x => x.Type)
        .IsInEnum()
        .DependentRules(() =>
        {
          RuleFor(x => x.Wheels)
            .NotEmpty();

          When(x => x.Type == VehicleType.Motorbike, () =>
          {
            RuleFor(x => x.Wheels)
              .Equal(2);
          }).Otherwise(() =>
          {
            RuleFor(x => x.Wheels)
              .Equal(4);
          });
        });
    }
  }
}