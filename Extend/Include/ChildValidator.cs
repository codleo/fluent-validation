using FluentValidation;

namespace Extend.Include
{
  public class ParentValidator : AbstractValidator<Parent>
  {
    public ParentValidator()
    {
      RuleFor(x => x.Name)
        .NotEmpty();
    }
  }

  public class ChildValidator : AbstractValidator<Child>
  {
    public ChildValidator()
    {
      Include(new ParentValidator());
    }
  }
}