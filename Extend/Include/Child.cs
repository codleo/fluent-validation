namespace Extend.Include
{
  public class Parent
  {
    public string Name { get; set; }
  }

  public class Child : Parent
  {
    public int Value { get; set; }
  }
}