﻿using System;
using Extend.Condition;
using Extend.CustomValidator;
using Extend.Include;
using FluentValidation;

namespace Extend
{
  class Program
  {
    static void Main(string[] args)
    {
      ValidatorOptions.LanguageManager.Enabled = false;

      // CustomValidatorCheck();
      // ConditionCheck();
      IncludeCheck();
    }

    static void CustomValidatorCheck()
    {
      var example = new CustomExample
      {
        GuidAsString = "invalid"
      };
      var validator = new CustomExampleValidator();

      var result = validator.Validate(example);

      Console.WriteLine(result);
    }

    static void ConditionCheck()
    {
      var vehicle = new Vehicle
      {
        Type = (VehicleType) 1,
      };
      var validator = new VehicleValidator();

      var result = validator.Validate(vehicle);

      Console.WriteLine(result);
    }

    static void IncludeCheck()
    {
      var child = new Child();
      var validator = new ChildValidator();

      var result = validator.Validate(child);

      Console.WriteLine(result);
    }
  }
}