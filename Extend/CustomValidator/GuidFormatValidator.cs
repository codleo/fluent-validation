using System;
using FluentValidation.Validators;

namespace Extend.CustomValidator
{
  public class GuidFormatValidator : PropertyValidator
  {
    public GuidFormatValidator() : base("Property {PropertyName} has invalid Guid format") { }

    protected override bool IsValid(PropertyValidatorContext context)
    {
      var value = (string) context.PropertyValue;
      Guid guid;
      return Guid.TryParse(value, out guid);
    }
  }

}