using FluentValidation;

namespace Extend.CustomValidator
{
  public class CustomExample
  {
    public string GuidAsString { get; set; }
  }

  public class CustomExampleValidator : AbstractValidator<CustomExample>
  {
    public CustomExampleValidator()
    {
        RuleFor(x => x.GuidAsString)
          .SetValidator(new GuidFormatValidator());
    }
  }
}