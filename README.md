# Fluent Validation

Fluent Validation examples presented on [blog](https://blog.codleo.pl/)

## Topics
### [Introduction](https://blog.codleo.pl/fluent-validation-introduction/)
### [Configuration](https://blog.codleo.pl/fluent-validation-configuration/)
### [Complex usage](https://blog.codleo.pl/fluent-validation-complex-usage/)
### [Tests](https://blog.codleo.pl/fluent-validation-tests/)
