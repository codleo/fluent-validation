using FluentValidation;

namespace Configuration
{
  public class ExampleValidator : AbstractValidator<Example>
  {
    public ExampleValidator()
    {
      CascadeMode = CascadeMode.StopOnFirstFailure;

      RuleFor(e => e.Number)
        .Cascade(CascadeMode.StopOnFirstFailure)
        .NotEmpty()
        .GreaterThan(10);

      RuleFor(e => e.Text)
        .NotEmpty()
        .WithMessage("You need to set value for {PropertyName}")
        .WithErrorCode("TextRequired");

      // Difficult to add localization resources for console app
      // .WithLocalizedMessage(typeof(MyMessages), "TextRequired")
    }
  }
}