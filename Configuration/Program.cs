﻿using System;
using System.Globalization;
using FluentValidation;

namespace Configuration
{
  class Program
  {
    static void Main(string[] args)
    {
      ValidatorOptions.LanguageManager.Enabled = false;
      ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;
      var example = new Example{
        Number = 0
      };

      var validator = new ExampleValidator();

      var result = validator.Validate(example);

      Console.WriteLine(result);

      // ErrorCode
      Console.WriteLine(result.Errors[1].ErrorCode);
    }
  }
}